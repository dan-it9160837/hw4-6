'use strict'
// #1 Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// поняття асинхронності у Javascript - це концепція, яка дозволяє виконувати код без очікування завершення певних операцій, щоб не чекати поки операція завершиться, 
// сторінка може продовжувати виконувати інші дії, і коли операція буде завершена, вона буде оброблена.
// #2  Написати програму "Я тебе знайду по IP"

const findIpBtn = document.getElementById('find-ip-btn');
const resultDiv = document.getElementById('result');

findIpBtn.addEventListener('click', async () => {
  try {
    const ipifyResponse = await fetch('https://api.ipify.org/?format=json');
    const ipifyData = await ipifyResponse.json();
    const ipAddress = ipifyData.ip;

    const ipApiResponse = await fetch(`https://ipapi.co/${ipAddress}/json/`);
    const ipApiData = await ipApiResponse.json();

    const address = `${ipApiData.country_name}, ${ipApiData.region}, ${ipApiData.city}, ${ipApiData.district}`;
    resultDiv.textContent = `Your address is: ${address}`;
  } catch (error) {
    console.error(error);
    resultDiv.textContent = 'An error occurred while fetching your address.';
  }
});